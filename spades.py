import random
import math

def spades_deck():
    """
    This function creates a deck of cards to be used in a game of spades

    Returns:
        ready_to_shuffle(list) - An indexed list of card tuples grouped by suit,
                                 in ascending order by face value and suit,
                                 with Spades being the highest indexed suit
    """
    ready_for_shuffle = []
    for num in range(1,53):
        if num < 14 :
            suit = "Hearts"
            if num < 10:
                face = num + 1
        if 13 < num < 27:
            suit = "Clubs"
            if 13 < num < 23:
                face = (num + 1) - 13
        if 26 < num < 40:
            suit = "Diamonds"
            if 26 < num < 36:
                face = (num + 1) - (13 * 2)
        if 39 < num < 53:
            suit = "Spades"
            if 39 < num < 49:
                face = (num + 1) - (13 * 3)
        if (num == 10 or num == 23 or num == 36 or num == 49):
            face = "Jack"
        if (num == 11 or num == 24 or num == 37 or num == 50):
            face = "Queen"
        if (num == 12 or num == 25 or num == 38 or num == 51):
            face = "King"
        if (num == 13 or num == 26 or num == 39 or num == 52):
            face = "Ace"
        card_point = (num, face, suit)
        ready_for_shuffle +=  [card_point]
    return ready_for_shuffle

def shuffled(deck):
    """
    This function shuffles a deck of cards

    Args:
        deck(list or tuples) - A list of card tuples to be shuffled

    Returns:
        deck(list of tuples) - A shuffled deck of cards
    """
    random.shuffle(deck)
    return deck

def spades_deal(deck):
    """
    This function deals out cards to 4 players playing spades from a shuffled
    deck

    Args:
        deck(list of tuples) - A deck of cards to be shuffled for spades

    Returns:
        p1(list of tuples) - A list of tuples for player 1, to be used as cards
        p2(list of tuples) - A list of tuples for player 2, to be used as cards
        p3(list of tuples) - A list of tuples for player 3, to be used as cards
        p4(list of tuples) - A list of tuples for player 4, to be used as cards
    """
    p1 = []
    p2 = []
    p3 = []
    p4 = []
    x = 0
    for i in deck:
        x = x + 1
        if x < 14:
            p1.append(i)
        if 13 < x < 27:
            p2.append(i)
        if 26 < x < 40:
            p3.append(i)
        if 39 < x < 53:
            p4.append(i)
    return sorted(p1), sorted(p2), sorted(p3), sorted(p4)
