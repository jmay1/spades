import math
import random
from spades import *
import pandas as pd

def bid(player):
    """
    This function serves to create automated bids for computer players in the
    game of spades

    Args:
        deal(multiple lists of tuples) - The playing cards for each player in
                                         a game of spades
    Returns:
        bid: An integer bid number
    """
    bid = 0
    spades = 0
    hearts = 0
    diamonds = 0
    clubs = 0
    for card in player:
        if card[0] in range(1,14):
            hearts = hearts + 1
        if card[0] in range(14,27):
            clubs = clubs + 1
        if card[0] in range(27,40):
            diamonds = diamonds + 1
        if card[0] in range(40,53):
            spades = spades + 1
        if (card[1] == "Ace"
            or card[1] == "King"):
            bid = bid + 1
    if hearts < 2 and spades > 2:
        bid = bid + 2
    if diamonds < 2 and spades > 2:
        bid = bid + 2
    if clubs < 2 and spades > 2:
        bid = bid + 2
    if (hearts + diamonds + clubs) < 7 and spades > 5:
        bid = bid + 1
    return bid

def initialize_spades(deck):
    """
    This function initiates a deck of cards intended for a game of spades,
    gives bids for 4 players, and returns a hand for each player

    Args:
        deck(list of tuples) - A shuffled list of sorted cards

    Returns:
        first_bid(int) - A randomized player set to bid and discard first
        p1(list of tuples) - A hand of cards for a player in spades
        p1_bid(int) - An automated bid number
        p2(list of tuples) - A hand of cards for a player in spades
        p2_bid(int) - An automated bid number
        p3(list of tuples) - A hand of cards for a player in spades
        p3_bid(int) - An automated bid number
        p4(list of tuples) - A hand of cards for a player in spades
        p4_bid(int) - An automated bid number
    """
    x = 0
    for i in deck: #Same structure could be used for
        x = x + 1
        if x == 1:
            p1 = i
            if (1,2,"Hearts") in p1:
                first_bid = 1
        if x == 2:
            p2 = i
            if (1,2,"Hearts") in p2:
                first_bid = 2
        if x == 3:
            p3 = i
            if (1,2,"Hearts") in p3:
                first_bid = 3
        if x == 4:
            p4 = i
            if (1,2,"Hearts") in p4:
                first_bid = 4
    if first_bid == 1:
        p1_bid = bid(p1)
        p2_bid = bid(p2)
        if p1_bid == 0:
            p3_bid = bid(p3) + 1
        if p1_bid > 0:
            p3_bid = bid(p3)
        if p2_bid == 0:
            p4_bid = bid(p4) + 1
        if p2_bid > 0:
            p4_bid = bid(p4)
        if p1_bid + p2_bid + p3_bid + p4_bid < 13:
            p4_bid = p4_bid + 1
        if p1_bid + p2_bid + p3_bid + p4_bid < 10:
            p4_bid = p4_bid + 1
        if p1_bid + p2_bid + p3_bid + p4_bid >= 13:
            p4_bid = p4_bid - 1
    if first_bid == 2:
        p2_bid = bid(p2)
        p3_bid = bid(p3)
        if p2_bid == 0:
            p4_bid = bid(p4) + 1
        if p2_bid > 0:
            p4_bid = bid(p4)
        if p3_bid == 0:
            p1_bid = bid(p1) + 1
        if p3_bid > 0:
            p1_bid = bid(p1)
        if p1_bid + p2_bid + p3_bid + p4_bid < 13:
            p1_bid = p1_bid + 1
        if p1_bid + p2_bid + p3_bid + p4_bid < 10:
            p1_bid = p1_bid + 1
        if p1_bid + p2_bid + p3_bid + p4_bid >= 14:
            p1_bid = p1_bid - 1
    if first_bid == 3:
        p3_bid = bid(p3)
        p4_bid = bid(p4)
        if p3_bid == 0:
            p1_bid = bid(p1) + 1
        if p3_bid > 0:
            p1_bid = bid(p1)
        if p4_bid == 0:
            p2_bid = bid(p2) + 1
        if p4_bid > 0:
            p2_bid = bid(p2)
        if p1_bid + p2_bid + p3_bid + p4_bid < 13:
            p2_bid = p2_bid + 1
        if p1_bid + p2_bid + p3_bid + p4_bid < 10:
            p2_bid = p2_bid + 1
        if p1_bid + p2_bid + p3_bid + p4_bid >= 14:
            p2_bid = p2_bid - 1
    if first_bid == 4:
        p4_bid = bid(p4)
        p1_bid = bid(p1)
        if p4_bid == 0:
            p2_bid = bid(p2) + 1
        if p4_bid > 0:
            p2_bid = bid(p2)
        if p1_bid == 0:
            p3_bid = bid(p3) + 1
        if p1_bid > 0:
            p3_bid = bid(p3)
        if p1_bid + p2_bid + p3_bid + p4_bid < 13:
            p3_bid = p3_bid + 1
        if p1_bid + p2_bid + p3_bid + p4_bid < 10:
            p3_bid = p3_bid + 1
        if p1_bid + p2_bid + p3_bid + p4_bid >= 13:
            p3_bid = p3_bid - 1
    return first_bid , p1, p1_bid, p2, p2_bid, p3, p3_bid, p4, p4_bid



def play_round():
    """
    This function is in progress, but separates a shuffled deck and bids,
    then assigns bids and hands to given variables

    Args:
        deck_bids(list of tuples) - A returned list of hands and bids for
                                    players in Spades
    """
    for i in range(1,14):
        deck_bids = initialize_spades(spades_deal(shuffled(spades_deck())))
        turn = deck_bids[0]
        p1_bid = deck_bids[2]
        p2_bid = deck_bids[4]
        p3_bid = deck_bids[6]
        p4_bid = deck_bids[8]
        if i == 1:
            spades_break = True
            p1 = set(deck_bids[1])
            p2 = set(deck_bids[3])
            p3 = set(deck_bids[5])
            p4 = set(deck_bids[7])
        round_frame = pd.DataFrame({"Bids":[p1_bid, p2_bid, p3_bid, p4_bid,\
        "nan"]}, index = ["Player 1", "Player 2", "Player 3", "Player 4","Winner"])
        hand_logic(i, turn, p1, p1_bid, p2, p2_bid, p3, p3_bid, p4, p4_bid, spades_break)

def hand_logic(frame, turn, p1_hand, p1_bid, p2_hand, p2_bid, p3_hand, p3_bid, p4_hand, p4_bid, spades_break):
    if turn == 1:
        p1_hand_disc = first_logic(p1, p1_bid, p3_bid, spades_break)
        p1 = p1_hand_disc[0]
        p1_disc = p1_hand_disc[1]
        set_suit = p1_disc[2]
        spades_break = p1_hand_disc[2]

        p2_hand_disc = second_logic(set_suit, p2, p2_bid, p4_bid, p1_disc, spades_break)
        p2 = p2_hand_disc[0]
        p2_disc = p2_hand_disc[1]

        p3_hand_disc = third_logic(set_suit, p3, p3_bid, p1_bid, p1_disc, p2_disc, spades_break)
        p3 = p3_hand_disc[0]
        p3_disc = p3_hand_disc[1]
        spades_break = p1_hand_disc[2]

        p4_hand_disc = fourth_logic(set_suit, p4, p4_bid, p2_bid, p1_disc, p3_disc, p3_disc, spades_break)
        p4_hand = p4_hand_disc[0]
        p4_disc = p4_hand_disc[1]
        spades_break = p4_hand_disc[2]



    if turn == 2:
        p2_hand_disc = first_logic(set_suit, p2, p2_bid, p4_bid, spades_break)
        p2 = p2_hand_disc[0]
        set_suit = p2_disc[2]
        p2_disc = p2_hand_disc[1]

        p3_hand_disc = second_logic(set_suit, p3, p3_bid, p1_bid, p2_disc,spades_break)
        p3 = p3_hand_disc[0]
        p3_disc = p3_hand_disc[1]
        spades_break = p1_hand_disc[2]

        p4_hand_disc = third_logic(set_suit, p4, p4_bid, p2_bid, p2_disc, p3_disc, spades_break)
        p4 = p4_hand_disc[0]
        p4_disc = p4_hand_disc[1]
        spades_break = p1_hand_disc[2]

        p1_hand_disc = fourth_logic(set_suit, p1, p1_bid, p3_bid, p3_disc, p2_disc, p4_disc, spades_break)
        p1_hand = p1_hand_disc[0]
        p1_disc = p1_hand_disc[1]
        spades_break = p1_hand_disc[2]

    if turn == 3:
        p3_hand_disc = first_logic(p3,p3_bid,p1_bid, spades_break)
        p3 = p3_hand_disc[0]
        p3_disc = p3_hand_disc[1]
        set_suit = p3_disc[2]
        spades_break = p3_hand_disc[2]

        p4_hand_disc = second_logic(set_suit, p4, p4_bid, p2_bid, p3_disc,spades_break)
        p4 = p4_hand_disc[0]
        p4_disc = p4_hand_disc[1]
        spades_break = p4_hand_disc[2]

        p1_hand_disc = third_logic(set_suit, p1, p1_bid, p3_bid, p3_disc, p4_disc, spades_break)
        p1 = p1_hand_disc[0]
        p1_disc = p1_hand_disc[1]
        spades_break = p1_hand_disc[2]

        p2_hand_disc = fourth_logic(set_suit, p2, p2_bid, p4_bid, p4_disc, p3_disc, p1_disc, spades_break)
        p2_hand = p2_hand_disc[0]
        p2_disc = p2_hand_disc[1]
        spades_break = p2_hand_disc[2]


    if turn == 4:
        p4_hand_disc = first_logic(p4,p4_bid,p2_bid, spades_break)
        p4 = p4_hand_disc[0]
        p4_disc = p4_hand_disc[1]
        set_suit = p4_disc[2]
        spades_break = p4_hand_disc[2]

        p1_hand_disc = second_logic(set_suit, p1, p1_bid, p3_bid, p4_disc,spades_break)
        p1 = p1_hand_disc[0]
        p1_disc = p1_hand_disc[1]
        spades_break = p1_hand_disc[2]

        p2_hand_disc = third_logic(set_suit, p2, p2_bid, p4_bid, p4_disc, p1_disc, spades_break)
        p2 = p2_hand_disc[0]
        p2_disc = p2_hand_disc[1]
        spades_break = p2_hand_disc[2]

        p3_hand_disc = fourth_logic(set_suit, p3, p3_bid, p1_bid, p1_disc, p4_disc, p2_disc, spades_break)
        p3_hand = p3_hand_disc[0]
        p3_disc = p3_hand_disc[1]
        spades_break = p3_hand_disc[2]





#first_logic(sorted(set(t[1])), t[2], t[6], True)
#second_logic(sorted(set(t[3])), t[4], t[8], last_disc, True)
#third_logic(team_discard, last_discard, player_hand, player_bid)
#fourth_logic(team_discard, opp1_discard, opp2_discard, player_hand, player_bid)
