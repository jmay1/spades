import math
import random
import pandas as pd
import numpy as np
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from spades_class import counter, Cards, Discard

def MyWindow():
    cards = Cards()
    hand = cards.p1
    length = len(hand)
    x = 80
    y = 800
    z = 800  #z to be used as an increment

    lx = 10
    ly = 50

    rx = 1100
    ry = 50

    tx = 250
    ty = 10
    app = QApplication(sys.argv)

    window = QWidget()

    window.setWindowTitle("Spades")
    window.setGeometry(100,100,1200,1000)

    if length > 0:
        card1 = QLabel(window)
        card1.setPixmap(QPixmap(str(hand[0][0])+".PNG"))
        if hand[0][2] == "Spades":
            card1.move(x, z)
        if hand[0][2] != "Spades":
            card1.move(x, 800)
        lc1 = QLabel(window)
        lc1.setPixmap(QPixmap("back.PNG"))
        lc1.move(lx, ly)

        rc1 = QLabel(window)
        rc1.setPixmap(QPixmap("back.PNG"))
        rc1.move(rx, ry)

        tc1 = QLabel(window)
        tc1.setPixmap(QPixmap("back2.PNG"))
        tc1.move(tx, ty)

    if length > 1:
        card2 = QLabel(window)
        card2.setPixmap(QPixmap(str(hand[1][0])+".PNG"))

        if hand[1][2] == "Spades":
            card2.move(x*2, z)
        if hand[1][2] != "Spades":
            card2.move(x*2, y)
        lc2 = QLabel(window)
        lc2.setPixmap(QPixmap("back.PNG"))
        lc2.move(lx, ly * 2)

        rc2 = QLabel(window)
        rc2.setPixmap(QPixmap("back.PNG"))
        rc2.move(rx, ry * 2)

        tc2 = QLabel(window)
        tc2.setPixmap(QPixmap("back2.PNG"))
        tc2.move(tx + 50, ty)

    if length > 2:
        card3 = QLabel(window)
        card3.setPixmap(QPixmap(str(hand[2][0])+".PNG"))

        if hand[2][2] == "Spades":
            card3.move(x*3, z)
        if hand[2][2] != "Spades":
            card3.move(x*3, y)
        lc3 = QLabel(window)
        lc3.setPixmap(QPixmap("back.PNG"))
        lc3.move(lx, ly * 3)

        rc3 = QLabel(window)
        rc3.setPixmap(QPixmap("back.PNG"))
        rc3.move(rx, ry * 3)

        tc3 = QLabel(window)
        tc3.setPixmap(QPixmap("back2.PNG"))
        tc3.move(tx + 100, ty)

    if length > 3:
        card4 = QLabel(window)
        card4.setPixmap(QPixmap(str(hand[3][0])+".PNG"))

        if hand[3][2] == "Spades":
            card4.move(x*4, z)
        if hand[3][2] != "Spades":
            card4.move(x*4, y)
        lc4 = QLabel(window)
        lc4.setPixmap(QPixmap("back.PNG"))
        lc4.move(lx, ly * 4)

        rc4 = QLabel(window)
        rc4.setPixmap(QPixmap("back.PNG"))
        rc4.move(rx, ry * 4)

        tc4 = QLabel(window)
        tc4.setPixmap(QPixmap("back2.PNG"))
        tc4.move(tx + 150, ty)

    if length > 4:
        card5 = QLabel(window)
        card5.setPixmap(QPixmap(str(hand[4][0])+".PNG"))

        card5.move(x*5,y)
        if hand[4][2] == "Spades":
            card5.move(x*5, z)
        if hand[4][2] != "Spades":
            card5.move(x*5, y)
        lc5 = QLabel(window)
        lc5.setPixmap(QPixmap("back.PNG"))
        lc5.move(lx, ly * 5)

        rc5 = QLabel(window)
        rc5.setPixmap(QPixmap("back.PNG"))
        rc5.move(rx, ry * 5)

        tc5 = QLabel(window)
        tc5.setPixmap(QPixmap("back2.PNG"))
        tc5.move(tx + 200, ty)

    if length > 5:
        card6 = QLabel(window)
        card6.setPixmap(QPixmap(str(hand[5][0])+".PNG"))

        if hand[5][2] == "Spades":
            card6.move(x*6, z)
        if hand[5][2] != "Spades":
            card6.move(x*6, y)
        lc6 = QLabel(window)
        lc6.setPixmap(QPixmap("back.PNG"))
        lc6.move(lx, ly * 6)

        rc6 = QLabel(window)
        rc6.setPixmap(QPixmap("back.PNG"))
        rc6.move(rx, ry * 6)

        tc6 = QLabel(window)
        tc6.setPixmap(QPixmap("back2.PNG"))
        tc6.move(tx + 250, ty)

    if length > 6:
        card7 = QLabel(window)
        card7.setPixmap(QPixmap(str(hand[6][0])+".PNG"))

        if hand[6][2] == "Spades":
            card7.move(x*7, z)
        if hand[6][2] != "Spades":
            card7.move(x*7,y)
        lc7 = QLabel(window)
        lc7.setPixmap(QPixmap("back.PNG"))
        lc7.move(lx, ly * 7)

        rc7 = QLabel(window)
        rc7.setPixmap(QPixmap("back.PNG"))
        rc7.move(rx, ry * 7)

        tc7 = QLabel(window)
        tc7.setPixmap(QPixmap("back2.PNG"))
        tc7.move(tx + 300, ty)

    if length > 7:
        card8 = QLabel(window)
        card8.setPixmap(QPixmap(str(hand[7][0])+".PNG"))

        if hand[7][2] == "Spades":
            card8.move(x*8, z)
        if hand[7][2] != "Spades":
            card8.move(x*8,y)
        lc8 = QLabel(window)
        lc8.setPixmap(QPixmap("back.PNG"))
        lc8.move(lx, ly * 8)

        rc8 = QLabel(window)
        rc8.setPixmap(QPixmap("back.PNG"))
        rc8.move(rx, ry * 8)

        tc8 = QLabel(window)
        tc8.setPixmap(QPixmap("back2.PNG"))
        tc8.move(tx + 350, ty)

    if length > 8:
        card9 = QLabel(window)
        card9.setPixmap(QPixmap(str(hand[8][0])+".PNG"))

        if hand[8][2] == "Spades":
            card9.move(x*9, z)
        if hand[8][2] != "Spades":
            card9.move(x*9,y)
        lc9 = QLabel(window)
        lc9.setPixmap(QPixmap("back.PNG"))
        lc9.move(lx, ly * 9)

        rc9 = QLabel(window)
        rc9.setPixmap(QPixmap("back.PNG"))
        rc9.move(rx, ry * 9)

        tc9 = QLabel(window)
        tc9.setPixmap(QPixmap("back2.PNG"))
        tc9.move(tx + 400, ty)

    if length > 9:
        card10 = QLabel(window)
        card10.setPixmap(QPixmap(str(hand[9][0])+".PNG"))

        if hand[9][2] == "Spades":
            card10.move(x*10, z)
        if hand[9][2] != "Spades":
            card10.move(x*10,y)
        lc10 = QLabel(window)
        lc10.setPixmap(QPixmap("back.PNG"))
        lc10.move(lx, ly * 10)

        rc10 = QLabel(window)
        rc10.setPixmap(QPixmap("back.PNG"))
        rc10.move(rx, ry * 10)

        tc10 = QLabel(window)
        tc10.setPixmap(QPixmap("back2.PNG"))
        tc10.move(tx + 450, ty)

    if length > 10:
        card11 = QLabel(window)
        card11.setPixmap(QPixmap(str(hand[10][0])+".PNG"))

        if hand[10][2] == "Spades":
            card11.move(x*11, z)
        if hand[10][2] != "Spades":
            card11.move(x*11,y)
        lc11 = QLabel(window)
        lc11.setPixmap(QPixmap("back.PNG"))
        lc11.move(lx, ly * 11)

        rc11 = QLabel(window)
        rc11.setPixmap(QPixmap("back.PNG"))
        rc11.move(rx, ry * 11)

        tc11 = QLabel(window)
        tc11.setPixmap(QPixmap("back2.PNG"))
        tc11.move(tx + 500, ty)

    if length > 11:
        card12 = QLabel(window)
        card12.setPixmap(QPixmap(str(hand[11][0])+".PNG"))

        if hand[11][2] == "Spades":
            card12.move(x*12, z)
        if hand[11][2] != "Spades":
            card12.move(x*12, y)
        lc12 = QLabel(window)
        lc12.setPixmap(QPixmap("back.PNG"))
        lc12.move(lx, ly * 12)

        rc12 = QLabel(window)
        rc12.setPixmap(QPixmap("back.PNG"))
        rc12.move(rx, ry * 12)

        tc12 = QLabel(window)
        tc12.setPixmap(QPixmap("back2.PNG"))
        tc12.move(tx + 550, ty)
    if length > 12:
        card13 = QLabel(window)
        card13.setPixmap(QPixmap(str(hand[12][0])+".PNG"))

        if hand[12][2] == "Spades":
            card13.move(x*13, z)
        if hand[12][2] != "Spades":
            card13.move(x*13, y)
        lc13 = QLabel(window)
        lc13.setPixmap(QPixmap("back.PNG"))
        lc13.move(lx, ly * 13)

        rc13 = QLabel(window)
        rc13.setPixmap(QPixmap("back.PNG"))
        rc13.move(rx, ry * 13)

        tc13 = QLabel(window)
        tc13.setPixmap(QPixmap("back2.PNG"))
        tc13.move(tx + 600, ty)

    tab1 = QWidget()
    tableWidget = QTableWidget(10, 10)

    tab1hbox = QHBoxLayout()
    tab1hbox.setContentsMargins(5, 5, 5, 5)
    tab1hbox.addWidget(tableWidget)
    tab1.setLayout(tab1hbox)
    window.show()

    sys.exit(app.exec())

MyWindow()
