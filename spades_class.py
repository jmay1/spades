import math
import random
import pandas as pd
import numpy as np
import tkinter as tk


from PIL import ImageTk, Image
from spades import *
from spades_apr_4 import *

def counter(player_hand):
    count_hearts = 0
    count_clubs = 0
    count_diamonds = 0
    count_spades = 0

    for i in player_hand:

        if "Hearts" in i:
            count_hearts = count_hearts + 1
        if "Clubs" in i:
            count_clubs = count_clubs + 1
        if "Diamonds" in i:
            count_diamonds = count_diamonds + 1
        if "Spades" in i:
            count_spades = count_spades + 1
    return count_hearts, count_clubs, count_diamonds, count_spades

class Cards:
    def __init__(self):
        deck = initialize_spades(spades_deal(shuffled(spades_deck())))

        self.turn = deck[0]
        self.spades_break = 1

        p1 = sorted(set(deck[1]))
        self.p1_c1 = p1[0]
        self.p1_c2 = p1[1]
        self.p1_c3 = p1[2]
        self.p1_c4 = p1[3]
        self.p1_c5 = p1[4]
        self.p1_c6 = p1[5]
        self.p1_c7 = p1[6]
        self.p1_c8 = p1[7]
        self.p1_c9 = p1[8]
        self.p1_c10 = p1[9]
        self.p1_c11 = p1[10]
        self.p1_c12 = p1[11]
        self.p1_c13 = p1[12]
        self.p1 = sorted({self.p1_c1, self.p1_c2, self.p1_c3, self.p1_c4,\
                             self.p1_c5, self.p1_c6, self.p1_c7, self.p1_c8,\
                             self.p1_c9, self.p1_c10, self.p1_c11, self.p1_c12,\
                             self.p1_c13})


        self.p1_bid = int(deck[2])


        self.p2 = sorted(set(deck[3]))
        self.p2_bid = int(deck[4])


        self.p3 = sorted(set(deck[5]))
        self.p3_bid = int(deck[6])


        self.p4 = sorted(set(deck[7]))
        self.p4_bid = int(deck[8])

        self.team1_bids = self.p1_bid, self.p3_bid
        self.team2_bids = self.p2_bid, self.p4_bid

        self.suits = {"Hearts", "Clubs", "Diamonds", "Spades"}

        self.score_frame = pd.DataFrame({"Bids":[0, 0, 0, 0,"nan"],"Wins":[0, 0, 0, 0, 0], "Bags":[0, 0, 0, 0, 0], "Score":[0,0,0,0,0]}, index = ["Player 1", "Player 2", "Player 3", "Player 4","Winner"])


    def add_frame(self, n, df, p1_disc, p2_disc, p3_disc, p4_disc, winner):
        """ This method is used to update a data frame with a new column, while accounting for the winner"""
        frame = pd.DataFrame({"Hand " + str(n):[p1_disc, p2_disc, p3_disc, p4_disc,        winner ]}, index = ["Player 1", "Player 2", "Player 3", "Player 4","Winner"])
        winner = frame["Hand " + str(n)].loc["Winner"]
        df["Wins"]["Player " + str(winner)] += 1

        return_frame = pd.concat([df, frame], axis = 1)
        return return_frame

    def add_calculation(self, df):
        """ This method is to be used at the end of a round to calculate the scores for both teams"""
        for num in range(1,4):
            if num == 3:
                return df
            elif num == 1:
                mate = 3
            elif num == 2:
                mate = 4

            wins = df["Wins"]["Player " + str(num)]
            bids = df["Bids"]["Player " + str(num)]
            bags = wins - bids

            mate_wins = df["Wins"]["Player " + str(mate)]
            mate_bids = df["Bids"]["Player " + str(mate)]
            mate_bags = mate_wins - mate_bids

            team_wins = wins + mate_wins
            team_bids = bids + mate_bids
            team_bags = bags + mate_bags


            if team_wins < team_bids and bids != 0 and mate_bids != 0:
                df["Score"]["Player " + str(num)] -= 50


            elif team_wins >= team_bids and bids != 0 and mate_bids != 0:
                df["Score"]["Player " + str(num)] +=  team_bids * 10
                df["Bags"]["Player " + str(num)] += team_bags


            elif team_wins == team_bids and bids != 0 and mate_bids != 0:
                df["Score"]["Player " + str(num)] +=  team_bids * 10
                df["Bags"]["Player " + str(num)] += 0


            elif bids == 0 and team_wins >= team_bids:
                if wins > 0:
                    df["Score"]["Player " + str(num)] +=  (team_bids * 10) - 100
                    df["Bags"]["Player " + str(num)] += team_bags

                elif wins == 0:
                    df["Score"]["Player " + str(num)] +=  (team_bids * 10) + 100
                    df["Bags"]["Player " + str(num)] += team_bags

            elif mate_bids == 0 and team_wins >= team_bids:
                if mate_wins > 0:
                    df["Score"]["Player " + str(num)] +=  (team_bids * 10) - 100
                    df["Bags"]["Player " + str(num)] += team_bags

                elif mate_wins == 0:
                    df["Score"]["Player " + str(num)] +=  (team_bids * 10) + 100
                    df["Bags"]["Player " + str(num)] += team_bags

            elif bids == 0 and team_wins <= team_bids:
                if wins > 0:
                    df["Score"]["Player " + str(num)] -=  150
                    df["Bags"]["Player " + str(num)] += team_bags

                elif wins == 0:
                    df["Score"]["Player " + str(num)] +=  50
                    df["Bags"]["Player " + str(num)] += team_bags

            elif mate_bids == 0 and team_wins <= team_bids:
                if mate_wins > 0:
                    df["Score"]["Player " + str(num)] -=  150
                    df["Bags"]["Player " + str(num)] += team_bags

                elif mate_wins == 0:
                    df["Score"]["Player " + str(num)] -=  150
                    df["Bags"]["Player " + str(num)] += team_bags


    def turn(self, p1, p2, p3, p4):
        """ This method determines who will open bidding/discarding at the
        beginning of a round"""
        for enum_, face, suit in self.p1:
            if face == 2 and suit == "Clubs":
                return 1
        for enum_, face, suit in self.p2:
            if face == 2 and suit == "Clubs":
                return 2
        for enum_, face, suit in self.p3:
            if face == 2 and suit == "Clubs":
                return 3
        for enum_, face, suit in self.p4:
            if face == 2 and suit == "Clubs":
                return 4

    def hand_call(self, p_hand):
        """ This method returns a sorted list of enum_erators for a hand,
        it is to be used in conjunction with GUI methods"""
        enums = []
        for enum_, face, suit in p_hand:
            enums.append(enum_)
        return sorted(set(enums))


    def hearts(self, p_hand):
        """ This method counts hearts, and will help in logic for discards"""
        count = counter(p_hand)
        return count[0]

    def clubs(self, p_hand):
        """ This method counts clubs, and will help in logic for discards"""
        count = counter(p_hand)
        return count[1]

    def diamonds(self, p_hand):
        """ This method counts diamonds, and will help in logic for discards"""
        count = counter(p_hand)
        return count[2]

    def spades(self, p_hand):
        """ This method counts spades, and will help in logic for discards"""
        count = counter(p_hand)
        return count[3]

    def max_suit(self, hearts, clubs, diamonds, spades):
        """ This method determines the suit with the most cards in a given hand"""
        if(hearts >= clubs and hearts >= diamonds and hearts >= spades):
            return "Hearts"
        elif(clubs >= hearts and clubs >= diamonds and clubs >= spades):
            return "Clubs"
        elif(diamonds >= hearts and diamonds >= clubs and diamonds >= spades):
            return "Diamonds"
        elif(spades >= hearts and spades >= clubs and spades >= diamonds):
            return "Spades"

    def min_suit(self, hearts, clubs, diamonds, spades):
        """ This method determines the suit with the least cards in a given hand"""
        if(hearts <= clubs and hearts <= diamonds and hearts <= spades):
            return "Hearts"
        elif(clubs <= hearts and clubs <= diamonds and clubs <= spades):
            return "Clubs"
        elif(diamonds <= hearts and diamonds <= clubs and diamonds <= spades):
            return "Diamonds"
        elif(spades <= hearts and spades <= clubs and spades <= diamonds):
            return "Spades"





class Discard(Cards):
    def __init__(self):
        Cards.__init__(self)
    def remove_card(self, player, card):
        self.player.remove(self.player.update())
        return str(self.player.card[0])
    def flog(self, p_hand, p_bid, mate_bid, min_suit, max_suit, spades_break):
        """ This method is to be used in determining an automated discard
        for a player taking the first turn

        Args:
            p_hand(list of tuples) - The hand of the player discarding
            p_bid(int) - The bid of the player discarding
            min_suit(str) - The suit with the least cards
            max_suit(str) - The suit with the most cards
            spades_break(int) - Either a 1 or 0, used for boolean purposes

        Returns:
            p_hand(list of tuples) - The cards in a player's hand with the discard removed
            discard(tuple) - An enum_erator, face value, and suit for the card discarded
            spades_break(int) - Either a 1 or 0, used for boolean purposes
            hand_call(set of string integers) - The left over enum_urators to be used in GUI environment
        """
        highs = ["Jack","Queen","King","Ace"]# These segments
        mids = [7,8,9,10]                    # are to be used with faces
        lows = [2,3,4,5,6]                   # in p_hand
        hand_call = self.hand_call(p_hand)
        while spades_break == 1:
            if p_bid == 0:#####################################################
                for enum_, face, suit in p_hand:
                    if (suit != "Spades"
                        and face in lows
                        or face in mids):
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call
                for enum_, face, suit in p_hand:
                    if (suit == max_suit):
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                    elif face in lows:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if face in mids or face in lows and suit != "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if face != "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if suit == "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

            elif p_bid != 0:###################################################
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if face in highs and suit != "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if (suit != "Spades"
                        and face in lows
                        or face in mids
                        or suit == min_suit):
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if suit == "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

        while spades_break == 0:
            if p_bid == 0:                                                #p_bid
                for enum_, face, suit in p_hand:
                    if (suit == min_suit and face in lows):
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                    elif (suit == max_suit and face not in highs):
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                        for enum_, face, suit in p_hand:
                            discard = (enum_, face, suit)
                            p_hand.remove(discard)
                            hand_call.remove(enum_)
                            return p_hand, discard, 0, hand_call

            if p_bid != 0:####################################  pbid  ##########
                for enum_, face, suit in p_hand:
                    if (suit == max_suit and face in highs):
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                    elif suit == min_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call
                for enum_, face, suit in p_hand:
                    discard = (enum_, face, suit)
                    p_hand.remove(discard)
                    hand_call.remove(enum_)
                    return p_hand, discard, 0, hand_call


    def slog(self, p_hand, p_bid, mate_bid, first_disc, spades_break):
        """ This method is to be used in determining an automated discard
        for a player taking the second turn

        Args:
            p_hand(list of tuples) - The hand of the player discarding
            p_bid(int) - The bid of the player discarding
            mate_bid(int) - The bid of the teammate for the player discarding
            first_disc(tuple) - The card discarded in the first turn
            spades_break(int) - Either a 1 or 0, used for boolean purposes

        Returns:
            p_hand(list of tuples) - The cards in a player's hand with the
                                     discard removed
            discard(tuple) - An enum_erator, face value, and suit for the card
                             discarded
            spades_break(int) - Either a 1 or 0, used for boolean purposes
            hand_call(set of string integers) - The left over enum_urators
                                                to be used in GUI environment
        """
        highs = ["Jack","Queen","King","Ace"]
        mids = [7,8,9,10]
        lows = [2,3,4,5,6]

        set_suit = first_disc[2]
        hand_call = self.hand_call(p_hand)

        while spades_break == 1:
            if p_bid != 0 and mate_bid:                                                #p_bid
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if (suit == set_suit and enum_ > first_disc[0]):
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call
                for enum_, face, suit in p_hand:
                    if suit == set_suit and enum_ < first_disc[0]:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                    elif suit == "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

            if p_bid == 0:                                                #pbid
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit and enum_ < first_disc[0]:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit != "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call
                for enum_, face, suit in p_hand:
                    if suit == "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

        while spades_break == 0:
            if p_bid != 0:
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit and enum_ > first_disc[0]:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                    elif suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    discard = (enum_, face, suit)
                    p_hand.remove(discard)
                    hand_call.remove(enum_)
                    return p_hand, discard, 0, hand_call

            if p_bid == 0:
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit and enum_ < first_disc[0]:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit != "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    discard = (enum_, face, suit)
                    p_hand.remove(discard)
                    hand_call.remove(enum_)
                    return p_hand, discard, 0, hand_call


    def tlog(self, p_hand, p_bid, mate_bid, mate_disc, opp_disc, spades_break):
        """ This method is to be used in determining an automated discard
        for a player taking the third turn

        Args:
            p_hand(list of tuples) - The hand of the player discarding
            p_bid(int) - The bid of the player discarding
            mate_bid(int) - The bid of the teammate for the player discarding
            mate_disc(tuple) - The card discarded in the first turn
            opp_disc(tuble) - The card discarded in the second turn
            spades_break(int) - Either a 1 or 0, used for boolean purposes

        Returns:
            p_hand(list of tuples) - The cards in a player's hand with the discard removed
            discard(tuple) - An enum_erator, face value, and suit for the card discarded
            spades_break(int) - Either a 1 or 0, used for boolean purposes
            hand_call(set of string integers) - The left over enum_urators to be used in GUI environment
        """
        highs = ["Jack","Queen","King","Ace"]
        mids = [7,8,9,10]
        lows = [2,3,4,5,6]

        set_suit = mate_disc[2]
        mate_enum_ = mate_disc[0]

        opp_suit = opp_disc[2]
        opp_enum_ = opp_disc[0]
        hand_call = self.hand_call(p_hand)

        while spades_break == 1:
            if (p_bid != 0
                and mate_enum_ < opp_enum_
                and set_suit != opp_suit):
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit and enum_ > opp_enum_:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call
                    elif suit == "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    discard = (enum_, face, suit)
                    p_hand.remove(discard)
                    hand_call.remove(enum_)
                    return p_hand, discard, 1, hand_call

            if p_bid != 0 and mate_enum_ > opp_enum_:
                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if suit != "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call
                    if suit == "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

            if p_bid == 0:
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit and enum_ < mate_enum_ or enum_ < opp_enum_:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if suit != "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                    if suit == "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

        while spades_break == 0:
            if (p_bid != 0
                and mate_enum_ < opp_enum_
                and set_suit == opp_suit):
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit and enum_ > opp_enum_:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    discard = (enum_, face, suit)
                    p_hand.remove(discard)
                    hand_call.remove(enum_)
                    return p_hand, discard, 0, hand_call
            elif (p_bid != 0
                  and mate_enum_ < opp_enum_
                  and opp_suit != set_suit):
                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call
                for enum_, face, suit in p_hand:
                    discard = (enum_, face, suit)
                    p_hand.remove(discard)
                    hand_call.remove(enum_)
                    return p_hand, discard, 0, hand_call

            elif p_bid != 0 and mate_enum_ > opp_enum_:
                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call
                for enum_, face, suit in p_hand:
                    discard = (enum_, face, suit)
                    p_hand.remove(discard)
                    hand_call.remove(enum_)
                    return p_hand, discard, 0, hand_call

            if p_bid == 0:
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit and enum_ < opp_enum_ or enum_ < mate_enum_:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    discard = (enum_, face, suit)
                    p_hand.remove(discard)
                    hand_call.remove(enum_)
                    return p_hand, discard, 0, hand_call

            for enum_, face, suit in p_hand:
                discard = (enum_, face, suit)
                p_hand.remove(discard)
                hand_call.remove(enum_)
                return p_hand, discard, 0, hand_call

    def four_log(self, p_hand, p_bid, mate_bid, mate_disc, opp_disc, opp2_disc,\
                 spades_break):
        """ This method is to be used in determining an automated discard
        for a player taking the third turn

        Args:
            p_hand(list of tuples) - The hand of the player discarding
            p_bid(int) - The bid of the player discarding
            mate_bid(int) - The bid of the teammate for the player discarding
            mate_disc(tuple) - The card discarded in the first turn
            opp_disc(tuble) - The card discarded in the second turn
            spades_break(int) - Either a 1 or 0, used for boolean purposes

        Returns:
            p_hand(list of tuples) - The cards in a player's hand with the discard removed
            discard(tuple) - An enum_erator, face value, and suit for the card discarded
            spades_break(int) - Either a 1 or 0, used for boolean purposes
            hand_call(set of string integers) - The left over enum_urators to be used in GUI environment
        """
        highs = ["Jack","Queen","King","Ace"]
        mids = [7,8,9,10]
        lows = [2,3,4,5,6]

        mate_suit = mate_disc[2]
        mate_enum_ = mate_disc[0]

        set_suit = opp_disc[2]
        opp_enum_ = opp_disc[0]
        opp2_suit = opp2_disc[2]
        opp2_enum_ = opp2_disc[0]

        hand_call = self.hand_call(p_hand)

        while spades_break == 1:
            if (p_bid != 0 and mate_enum_ < opp_enum_ or mate_enum_ < opp2_enum_):
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit and enum_ > opp_enum_ and enum_ > opp2_enum_ :
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if suit == set_suit and enum_ < opp_enum_ and enum_ < opp2_enum_:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call
            if p_bid != 0:
                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call
                for enum_, face, suit in p_hand:
                    if suit != "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if suit == "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

            if (p_bid != 0
                and mate_enum_ > opp_enum_
                and mate_enum_ > opp_enum_
                or mate_suit == set_suit):
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit and enum_ < mate_enum_:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call
                for enum_, face, suit in p_hand:
                    if suit != "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                    if suit == "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call


            if p_bid == 0:
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if (suit == set_suit
                        and (enum_ < opp_enum_
                        or enum_ < mate_enum_
                        or enum_ < opp2_enum_)):
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call

                for enum_, face, suit in p_hand:
                    if suit != "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 1, hand_call
                    if suit == "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

        while spades_break == 0:
            if p_bid != 0 and mate_enum_ < opp_enum_ or mate_enum_ < opp2_enum_:
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit and enum_ > opp_enum_:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    discard = (enum_, face, suit)
                    p_hand.remove(discard)
                    hand_call.remove(enum_)
                    return p_hand, discard, 0, hand_call

            if p_bid != 0 and mate_enum_ > opp_enum_ and mate_enum_ > opp2_enum_:
                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call
                for enum_, face, suit in p_hand:
                    if suit == "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    discard = (enum_, face, suit)
                    p_hand.remove(discard)
                    hand_call.remove(enum_)
                    return p_hand, discard, 0, hand_call

            if p_bid == 0:
                for enum_, face, suit in sorted(p_hand, reverse = True):
                    if suit == set_suit and enum_ < opp_enum_ or enum_ < mate_enum_ or enum_ < opp2_enum_:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    if suit == set_suit:
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call

                for enum_, face, suit in p_hand:
                    if suit != "Spades":
                        discard = (enum_, face, suit)
                        p_hand.remove(discard)
                        hand_call.remove(enum_)
                        return p_hand, discard, 0, hand_call
                for enum_, face, suit in p_hand:
                    discard = (enum_, face, suit)
                    p_hand.remove(discard)
                    hand_call.remove(enum_)
                    return p_hand, discard, 0, hand_call

            for enum_, face, suit in p_hand:
                discard = (enum_, face, suit)
                p_hand.remove(discard)
                hand_call.remove(enum_)
                return p_hand, discard, 0, hand_call


    def winner(self, p1_disc, p2_disc, p3_disc, p4_disc, set_suit):
        """ This method determines the winner based on 4 discards
        Args:
            p1_disc(tuple) - Discard from first turn
            p2_disc(tuple) - Discard from second turn
            p3_disc(tuple) - Discard from third turn
            p4_disc(tuple) - Discard from fourth turn

        Returns:
            winner(int) - An integer 1-4 representing the player that won"""
        for a, b, c in sorted(set([p1_disc, p2_disc, p3_disc, p4_disc]), reverse = True):
            if c == set_suit or c == "Spades":
                if (a,b,c) == p1_disc:
                    winner = 1
                if(a,b,c) == p2_disc:
                    winner = 2
                if(a,b,c) == p3_disc:
                    winner = 3
                if(a,b,c) == p4_disc:
                    winner = 4
                return winner
