# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\JoshW\Desktop\Obj_Or_Prog\spades_prog_gui.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!
from spades_class import Cards, Discard, counter
from spades import *
from spades_apr_4 import *

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    """ This class creates a GUI environment for the card game 'Spades'

        PCA - Pay Close Attention to patterns in PyQt5
            eg.
               ... = QtWidgets.QtWidget(...)
               ....setObjectName("...")
               ....setGeometry(QtCore.QRect(longAnch, latAnch, Diagonal, Width))
               ....setText("..")
               ....setPixmap(QtGui.QPixmap(".....PNG")
               ....raise_()

            """
    def setupUi(self, MainWindow):# Create window
        MainWindow.setObjectName("SpadesWindow") # Name window
        MainWindow.resize(1122, 944)# Size window
        MainWindow.setAutoFillBackground(False)# ...
        MainWindow.setStyleSheet("")# ...

        self.centralwidget = QtWidgets.QWidget(MainWindow) # ID main widget
        self.centralwidget.setObjectName("centralwidget") # Name main widget

        self.background = QtWidgets.QLabel(self.centralwidget)
        self.background.setGeometry(QtCore.QRect(-40, -60, 1261, 971))
        self.background.setText("")
        self.background.setPixmap(QtGui.QPixmap("background.png"))
        self.background.setObjectName("background")
        self.background.raise_()

        self.deck = Cards()
        self.disc_test = Discard()

        self.bid1 = self.deck.p1_bid
        self.bid2 = self.deck.p2_bid
        self.bid3 = self.deck.p3_bid
        self.bid4 = self.deck.p4_bid

        self.round_score = QtWidgets.QTableWidget(self.centralwidget)
        self.round_score.setGeometry(QtCore.QRect(170, 690, 891, 161))
        self.round_score.setMaximumSize(QtCore.QSize(931, 16777215))

        font = QtGui.QFont()
        font.setPointSize(7)

        self.round_score.setFont(font)
        self.round_score.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.round_score.setAutoFillBackground(False)
        self.round_score.setStyleSheet("")
        self.round_score.setVerticalScrollBarPolicy(QtCore.\
                                                    Qt.ScrollBarAlwaysOff)

        self.round_score.setHorizontalScrollBarPolicy(QtCore.\
                                                      Qt.ScrollBarAlwaysOff)

        self.round_score.setAutoScroll(False)
        self.round_score.setAutoScrollMargin(14)
        self.round_score.setAlternatingRowColors(True)
        self.round_score.setSelectionMode(QtWidgets.QAbstractItemView.\
                                                    ExtendedSelection)
        self.round_score.setSelectionBehavior(QtWidgets.\
                                              QAbstractItemView.SelectRows)
        self.round_score.setGridStyle(QtCore.Qt.SolidLine)
        self.round_score.setWordWrap(False)
        self.round_score.setCornerButtonEnabled(False)
        self.round_score.setRowCount(5)
        self.round_score.setColumnCount(15)
        self.round_score.setObjectName("round_score")
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setVerticalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setVerticalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setVerticalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setVerticalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setVerticalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(5, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(6, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(7, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(8, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(9, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(10, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(11, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(12, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(13, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.round_score.setHorizontalHeaderItem(14, item)
        item = QtWidgets.QTableWidgetItem()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        item.setBackground(brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        item.setForeground(brush)

        self.round_score.setItem(4, 0, item)
        item = QtWidgets.QTableWidgetItem()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        item.setBackground(brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        item.setForeground(brush)

        self.round_score.setItem(4, 1, item)
        self.round_score.horizontalHeader().setVisible(True)
        self.round_score.horizontalHeader().setCascadingSectionResizes(False)
        self.round_score.horizontalHeader().setDefaultSectionSize(55)
        self.round_score.horizontalHeader().setMinimumSectionSize(60)
        self.round_score.verticalHeader().setDefaultSectionSize(25)


        item = QtWidgets.QTableWidgetItem(str(self.bid1))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(False)
        font.setWeight(75)
        item.setFont(font)
        self.round_score.setItem(0,0, item)

        item = QtWidgets.QTableWidgetItem(str(self.bid2))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(False)
        font.setWeight(75)
        item.setFont(font)
        self.round_score.setItem(1,0, item)

        item = QtWidgets.QTableWidgetItem(str(self.bid3))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(False)
        font.setWeight(75)
        item.setFont(font)
        self.round_score.setItem(2,0, item)

        item = QtWidgets.QTableWidgetItem(str(self.bid3))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(False)
        font.setWeight(75)
        item.setFont(font)
        self.round_score.setItem(3,0, item)

        for i in range(1,14):
            self.discard_box = QtWidgets.QGroupBox(self.centralwidget)
            self.discard_box.setGeometry(QtCore.QRect(299, 109, 491, 411))
            self.discard_box.setTitle("")
            self.discard_box.setObjectName("discard_box")
            self.p3_disc_spot = QtWidgets.QWidget(self.discard_box)
            self.p3_disc_spot.setGeometry(QtCore.QRect(380, 140, 81, 111))
            self.p3_disc_spot.setStyleSheet("border-color: rgb(25, 25, 25);\n"
            "background-color: rgb(118, 118, 118);")
            self.p3_disc_spot.setObjectName("p3_disc_spot")
            self.gridLayout = QtWidgets.QGridLayout(self.p3_disc_spot)
            self.gridLayout.setContentsMargins(0, 0, 0, 0)
            self.gridLayout.setObjectName("gridLayout")
            self.p1_disc_spot = QtWidgets.QWidget(self.discard_box)
            self.p1_disc_spot.setGeometry(QtCore.QRect(210, 270, 81, 111))
            self.p1_disc_spot.setStyleSheet("border-color: rgb(25, 25, 25);\n"
            "background-color: rgb(118, 118, 118);")
            self.p1_disc_spot.setObjectName("self.p1_disc_spot")
            self.gridLayout_3 = QtWidgets.QGridLayout(self.p1_disc_spot)
            self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
            self.gridLayout_3.setObjectName("gridLayout_3")
            self.p2_disc_spot = QtWidgets.QWidget(self.discard_box)
            self.p2_disc_spot.setGeometry(QtCore.QRect(30, 140, 81, 111))
            self.p2_disc_spot.setStyleSheet("border-color: rgb(25, 25, 25);\n"
            "background-color: rgb(118, 118, 118);")
            self.p2_disc_spot.setObjectName("p2_disc_spot")
            self.gridLayout_4 = QtWidgets.QGridLayout(self.p2_disc_spot)
            self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
            self.gridLayout_4.setObjectName("gridLayout_4")
            self.p4_disc_spot = QtWidgets.QWidget(self.discard_box)
            self.p4_disc_spot.setGeometry(QtCore.QRect(210, 40, 81, 111))
            self.p4_disc_spot.setStyleSheet("border-color: rgb(25, 25, 25);\n"
            "background-color: rgb(118, 118, 118);")
            self.p4_disc_spot.setObjectName("p4_disc_spot")
            self.gridLayout_5 = QtWidgets.QGridLayout(self.p4_disc_spot)
            self.gridLayout_5.setContentsMargins(0, 0, 0, 0)
            self.gridLayout_5.setObjectName("gridLayout_5")

            if i == 1:
                self.p1 = self.deck.p1
                self.p2 = self.deck.p2
                self.p3 = self.deck.p3
                self.p4 = self.deck.p4

                self.p1_c1 = self.deck.p1_c1
                self.p1_c2 = self.deck.p1_c2
                self.p1_c3 = self.deck.p1_c3
                self.p1_c4 = self.deck.p1_c4
                self.p1_c5 = self.deck.p1_c5
                self.p1_c6 = self.deck.p1_c6
                self.p1_c7 = self.deck.p1_c7
                self.p1_c8 = self.deck.p1_c8
                self.p1_c9 = self.deck.p1_c9
                self.p1_c10 = self.deck.p1_c10
                self.p1_c11 = self.deck.p1_c11
                self.p1_c12 = self.deck.p1_c12
                self.p1_c13 = self.deck.p1_c13

                spades_break = self.deck.spades_break
                turn = self.deck.turn

                hearts = self.deck.hearts(self.p1)
                clubs = self.deck.clubs(self.p1)
                diamonds = self.deck.diamonds(self.p1)
                spades = self.deck.spades(self.p1)

                hearts2 = self.deck.hearts(self.p2)
                clubs2 = self.deck.clubs(self.p2)
                diamonds2 = self.deck.diamonds(self.p2)
                spades2 = self.deck.spades(self.p2)

                hearts3 = self.deck.hearts(self.p3)
                clubs3 = self.deck.clubs(self.p3)
                diamonds3 = self.deck.diamonds(self.p3)
                spades3 = self.deck.spades(self.p3)

                hearts4 = self.deck.hearts(self.p4)
                clubs4 = self.deck.clubs(self.p4)
                diamonds4 = self.deck.diamonds(self.p4)
                spades4 = self.deck.spades(self.p4)

                min_suit_1 = self.deck.min_suit(hearts, clubs, diamonds, spades)
                max_suit_1 = self.deck.max_suit(hearts, clubs, diamonds, spades)

                min_suit_2 = self.deck.min_suit(hearts2, clubs2, diamonds2, spades2)
                max_suit_2 = self.deck.max_suit(hearts2, clubs2, diamonds2, spades2)

                min_suit_3 = self.deck.min_suit(hearts3, clubs3, diamonds3, spades3)
                max_suit_3 = self.deck.max_suit(hearts3, clubs3, diamonds3, spades3)

                min_suit_4 = self.deck.min_suit(hearts4, clubs4, diamonds4, spades4)
                max_suit_4 = self.deck.max_suit(hearts4, clubs4, diamonds4, spades4)

                self.p1cards = QtWidgets.QGroupBox(self.centralwidget)
                self.p1cards.setGeometry(QtCore.QRect(10, 550, 1051, 141))
                self.p1cards.setObjectName("self.p1cards")

                x = 0
                spacer_pivot = 10
                spacer = 80

                if self.p1_c1:
                    card1_icon = QtGui.QIcon()
                    card1_icon.addPixmap(QtGui.QPixmap(str(self.p1_c1[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card1 = QtWidgets.QPushButton(self.p1cards)
                    self.card1.setGeometry(QtCore.QRect(10, 20, 71, 101))

                    self.card1.setIcon(card1_icon)
                    self.card1.setIconSize(QtCore.QSize(256, 256))
                    self.card1.setObjectName(str(self.p1_c1))
                    self.card1.raise_()
                    self.card1.clicked.connect(lambda:
                                         self.on_click(index = self.card1,  \
                                                       p_hand = self.p1,\
                                                       cardex = self.p1_c1))

                if self.p1_c2:
                    card2_icon = QtGui.QIcon()
                    card2_icon.addPixmap(QtGui.QPixmap(str(self.p1_c2[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card2 = QtWidgets.QPushButton(self.p1cards)
                    self.card2.setGeometry(QtCore.QRect(1 * spacer + 10,\
                                                              20, 71, 101))

                    self.card2.setIcon(card2_icon)
                    self.card2.setIconSize(QtCore.QSize(256, 256))
                    self.card2.setObjectName(str(self.p1_c2))
                    self.card2.raise_()
                    self.card2.clicked.connect(lambda:
                                         self.on_click(index = self.card2,  \
                                                       p_hand = self.p1,\
                                                       cardex = self.p1_c2))
                if self.p1_c3:
                    card3_icon = QtGui.QIcon()
                    card3_icon.addPixmap(QtGui.QPixmap(str(self.p1_c3[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card3 = QtWidgets.QPushButton(self.p1cards)
                    self.card3.setGeometry(QtCore.QRect(2 * spacer + 10,\
                                                              20, 71, 101))

                    self.card3.setIcon(card3_icon)
                    self.card3.setIconSize(QtCore.QSize(256, 256))
                    self.card3.setObjectName(str(self.p1_c3))
                    self.card3.raise_()
                    self.card3.clicked.connect(lambda:
                                        self.on_click(index = self.card3,  \
                                                      p_hand = self.p1,\
                                                      cardex = self.p1_c3))
                if self.p1_c4:
                    card4_icon = QtGui.QIcon()
                    card4_icon.addPixmap(QtGui.QPixmap(str(self.p1_c4[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card4 = QtWidgets.QPushButton(self.p1cards)
                    self.card4.setGeometry(QtCore.QRect(3 * spacer + 10,\
                                                              20, 71, 101))

                    self.card4.setIcon(card4_icon)
                    self.card4.setIconSize(QtCore.QSize(256, 256))
                    self.card4.setObjectName(str(self.p1_c4))
                    self.card4.raise_()
                    self.card4.clicked.connect(lambda:
                                        self.on_click(index = self.card4,  \
                                                      p_hand = self.p1,\
                                                      cardex = self.p1_c4))
                if self.p1_c5:
                    card5_icon = QtGui.QIcon()
                    card5_icon.addPixmap(QtGui.QPixmap(str(self.p1_c5[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card5 = QtWidgets.QPushButton(self.p1cards)
                    self.card5.setGeometry(QtCore.QRect(4 * spacer + 10,\
                                                              20, 71, 101))

                    self.card5.setIcon(card5_icon)
                    self.card5.setIconSize(QtCore.QSize(256, 256))
                    self.card5.setObjectName(str(self.p1_c5))
                    self.card5.raise_()
                    self.card5.clicked.connect(lambda:
                                        self.on_click(index = self.card5,  \
                                                      p_hand = self.p1,\
                                                      cardex = self.p1_c5))
                if self.p1_c6:
                    card6_icon = QtGui.QIcon()
                    card6_icon.addPixmap(QtGui.QPixmap(str(self.p1_c6[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card6 = QtWidgets.QPushButton(self.p1cards)
                    self.card6.setGeometry(QtCore.QRect(5 * spacer + 10,\
                                                              20, 71, 101))

                    self.card6.setIcon(card6_icon)
                    self.card6.setIconSize(QtCore.QSize(256, 256))
                    self.card6.setObjectName(str(self.p1_c6))
                    self.card6.raise_()
                    self.card6.clicked.connect(lambda:
                                        self.on_click(index = self.card6,  \
                                                      p_hand = self.p1,\
                                                      cardex = self.p1_c6))
                if self.p1_c7:
                    card7_icon = QtGui.QIcon()
                    card7_icon.addPixmap(QtGui.QPixmap(str(self.p1_c7[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card7 = QtWidgets.QPushButton(self.p1cards)
                    self.card7.setGeometry(QtCore.QRect(6 * spacer + 10,\
                                                              20, 71, 101))

                    self.card7.setIcon(card7_icon)
                    self.card7.setIconSize(QtCore.QSize(256, 256))
                    self.card7.setObjectName(str(self.p1_c7))
                    self.card7.raise_()
                    self.card7.clicked.connect(lambda:
                                        self.on_click(index = self.card7,  \
                                                      p_hand = self.p1,\
                                                      cardex = self.p1_c7))
                if self.p1_c8:
                    card8_icon = QtGui.QIcon()
                    card8_icon.addPixmap(QtGui.QPixmap(str(self.p1_c8[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card8 = QtWidgets.QPushButton(self.p1cards)
                    self.card8.setGeometry(QtCore.QRect(7 * spacer + 10,\
                                                              20, 71, 101))

                    self.card8.setIcon(card8_icon)
                    self.card8.setIconSize(QtCore.QSize(256, 256))
                    self.card8.setObjectName(str(self.p1_c8))
                    self.card8.raise_()
                    self.card8.clicked.connect(lambda:
                                        self.on_click(index = self.card8,  \
                                                      p_hand = self.p1,\
                                                      cardex = self.p1_c8))
                if self.p1_c9:
                    card9_icon = QtGui.QIcon()
                    card9_icon.addPixmap(QtGui.QPixmap(str(self.p1_c9[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card9 = QtWidgets.QPushButton(self.p1cards)
                    self.card9.setGeometry(QtCore.QRect(8 * spacer + 10,\
                                                              20, 71, 101))

                    self.card9.setIcon(card9_icon)
                    self.card9.setIconSize(QtCore.QSize(256, 256))
                    self.card9.setObjectName(str(self.p1_c9))
                    self.card9.raise_()
                    self.card9.clicked.connect(lambda:
                                         self.on_click(index = self.card9,  \
                                                       p_hand = self.p1,\
                                                       cardex = self.p1_c9))
                if self.p1_c10:
                    card10_icon = QtGui.QIcon()
                    card10_icon.addPixmap(QtGui.QPixmap(str(self.p1_c10[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card10 = QtWidgets.QPushButton(self.p1cards)
                    self.card10.setGeometry(QtCore.QRect(9 * spacer + 10,\
                                                              20, 71, 101))

                    self.card10.setIcon(card10_icon)
                    self.card10.setIconSize(QtCore.QSize(256, 256))
                    self.card10.setObjectName(str(self.p1_c10))
                    self.card10.raise_()
                    self.card10.clicked.connect(lambda:
                                         self.on_click(index = self.card10, \
                                                       p_hand = self.p1,\
                                                       cardex = self.p1_c10))
                if self.p1_c11:
                    card11_icon = QtGui.QIcon()
                    card11_icon.addPixmap(QtGui.QPixmap(str(self.p1_c11[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card11 = QtWidgets.QPushButton(self.p1cards)
                    self.card11.setGeometry(QtCore.QRect(10 * spacer + 10,\
                                                              20, 71, 101))

                    self.card11.setIcon(card11_icon)
                    self.card11.setIconSize(QtCore.QSize(256, 256))
                    self.card11.setObjectName(str(self.p1_c11))
                    self.card11.raise_()
                    self.card11.clicked.connect(lambda:
                                         self.on_click(index = self.card11, \
                                                       p_hand = self.p1,\
                                                       cardex = self.p1_c11))
                if self.p1_c12:
                    card12_icon = QtGui.QIcon()
                    card12_icon.addPixmap(QtGui.QPixmap(str(self.p1_c12[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card12 = QtWidgets.QPushButton(self.p1cards)
                    self.card12.setGeometry(QtCore.QRect(11 * spacer + 10,\
                                                              20, 71, 101))

                    self.card12.setIcon(card12_icon)
                    self.card12.setIconSize(QtCore.QSize(256, 256))
                    self.card12.setObjectName(str(self.p1_c12))
                    self.card12.raise_()
                    self.card12.clicked.connect(lambda:
                                         self.on_click(index = self.card12, \
                                                       p_hand = self.p1,\
                                                       cardex = self.p1_c12))
                if self.p1_c13:
                    card13_icon = QtGui.QIcon()
                    card13_icon.addPixmap(QtGui.QPixmap(str(self.p1_c13[0])+".PNG"),
                                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    self.card13 = QtWidgets.QPushButton(self.p1cards)
                    self.card13.setGeometry(QtCore.QRect(12 * spacer + 10,\
                                                              20, 71, 101))

                    self.card13.setIcon(card13_icon)
                    self.card13.setIconSize(QtCore.QSize(256, 256))
                    self.card13.setObjectName(str(self.p1_c13))
                    self.card13.raise_()
                    self.card13.clicked.connect(lambda:
                                         self.on_click(index = self.card13, \
                                                       p_hand = self.p1,\
                                                       cardex = self.p1_c13))
                x = 0
                for card in self.p2:
                    spacer = 30
                    self.p2_cards = QtWidgets.QGroupBox(self.centralwidget)
                    self.p2_cards.setGeometry(QtCore.QRect(90, 20, 171, 531))
                    self.p2_cards.setTitle("")
                    self.p2_cards.setObjectName("p2_cards")
                    x += 1
                    self.label = QtWidgets.QLabel(self.p2_cards)
                    self.label.setGeometry(QtCore.QRect(60, x * spacer, 101, 71))
                    self.label.setText("")
                    self.label.setPixmap(QtGui.QPixmap("back.png"))
                    self.label.raise_()

                x = 0
                for card in self.p3:
                    self.p3_cards = QtWidgets.QGroupBox(self.centralwidget)
                    self.p3_cards.setGeometry(QtCore.QRect(300, 0, 491, 111))
                    self.p3_cards.setTitle("")
                    self.p3_cards.setObjectName("p3_cards")
                    x += 1
                    self.label = QtWidgets.QLabel(self.p3_cards)
                    self.label.setGeometry(QtCore.QRect(30 * x, 10, 71, 101))
                    self.label.setText("")
                    self.label.setPixmap(QtGui.QPixmap("back2.png"))

                    self.label.raise_()
                x = 0
                for card in self.p4:
                    self.p4_cards = QtWidgets.QGroupBox(self.centralwidget)
                    self.p4_cards.setGeometry(QtCore.QRect(820, 30, 171, 531))
                    self.p4_cards.setTitle("")
                    self.p4_cards.setObjectName("p4_cards")
                    x += 1
                    meas = 20 + (x * 30)
                    self.label = QtWidgets.QLabel(self.p4_cards)
                    self.label.setGeometry(QtCore.QRect(10, meas, 101, 71))
                    self.label.setText("")
                    self.label.setPixmap(QtGui.QPixmap("back.png"))
                    self.label.raise_()


        self.master_score = QtWidgets.QTableWidget(self.centralwidget)
        self.master_score.setGeometry(QtCore.QRect(10, 740, 151, 111))
        font = QtGui.QFont()
        font.setPointSize(7)
        self.master_score.setFont(font)
        self.master_score.setAutoFillBackground(False)
        self.master_score.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.master_score.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.master_score.setAutoScroll(False)
        self.master_score.setTabKeyNavigation(False)
        self.master_score.setProperty("showDropIndicator", False)
        self.master_score.setAlternatingRowColors(True)
        self.master_score.setGridStyle(QtCore.Qt.SolidLine)
        self.master_score.setCornerButtonEnabled(False)
        self.master_score.setRowCount(2)
        self.master_score.setColumnCount(2)
        self.master_score.setObjectName("master_score")

        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.master_score.setVerticalHeaderItem(0, item)

        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)

        self.master_score.setVerticalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.master_score.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.master_score.setHorizontalHeaderItem(1, item)
        self.master_score.horizontalHeader().setVisible(True)
        self.master_score.horizontalHeader().setCascadingSectionResizes(False)
        self.master_score.horizontalHeader().setDefaultSectionSize(45)
        self.master_score.horizontalHeader().setMinimumSectionSize(40)
        self.master_score.verticalHeader().setVisible(True)
        self.master_score.verticalHeader().setDefaultSectionSize(40)

        self.p1cards.raise_()
        self.discard_box.raise_()
        self.p2_cards.raise_()
        self.p3_cards.raise_()
        self.master_score.raise_()
        self.round_score.raise_()
        self.p4_cards.raise_()
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1122, 26))
        self.menubar.setObjectName("menubar")
        self.menuSpades = QtWidgets.QMenu(self.menubar)
        self.menuSpades.setObjectName("menuSpades")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menubar.addAction(self.menuSpades.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def on_click(self, index, p_hand, cardex):
        self.p1cards.index.clear(index)





    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.p1cards.setTitle(_translate("MainWindow", "Your Cards"))
        item = self.master_score.verticalHeaderItem(0)
        item.setText(_translate("MainWindow", "Team1"))
        item = self.master_score.verticalHeaderItem(1)
        item.setText(_translate("MainWindow", "Team2"))
        item = self.master_score.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Score"))
        item = self.master_score.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Bags"))
        self.round_score.setSortingEnabled(False)
        item = self.round_score.verticalHeaderItem(0)
        item.setText(_translate("MainWindow", "Player 1"))
        item = self.round_score.verticalHeaderItem(1)
        item.setText(_translate("MainWindow", "Player 2"))
        item = self.round_score.verticalHeaderItem(2)
        item.setText(_translate("MainWindow", "Player 3"))
        item = self.round_score.verticalHeaderItem(3)
        item.setText(_translate("MainWindow", "Player 4"))
        item = self.round_score.verticalHeaderItem(4)
        item.setText(_translate("MainWindow", "Winner"))
        item = self.round_score.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Bid"))
        item = self.round_score.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Wins"))
        item = self.round_score.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Rnd 1"))
        item = self.round_score.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "Rnd 2"))
        item = self.round_score.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow", "Rnd 3"))
        item = self.round_score.horizontalHeaderItem(5)
        item.setText(_translate("MainWindow", "Rnd 4"))
        item = self.round_score.horizontalHeaderItem(6)
        item.setText(_translate("MainWindow", "Rnd 5"))
        item = self.round_score.horizontalHeaderItem(7)
        item.setText(_translate("MainWindow", "Rnd 6"))
        item = self.round_score.horizontalHeaderItem(8)
        item.setText(_translate("MainWindow", "Rnd 7"))
        item = self.round_score.horizontalHeaderItem(9)
        item.setText(_translate("MainWindow", "Rnd 8"))
        item = self.round_score.horizontalHeaderItem(10)
        item.setText(_translate("MainWindow", "Rnd 9"))
        item = self.round_score.horizontalHeaderItem(11)
        item.setText(_translate("MainWindow", "Rnd 10"))
        item = self.round_score.horizontalHeaderItem(12)
        item.setText(_translate("MainWindow", "Rnd 11"))
        item = self.round_score.horizontalHeaderItem(13)
        item.setText(_translate("MainWindow", "Rnd 12"))
        item = self.round_score.horizontalHeaderItem(14)
        item.setText(_translate("MainWindow", "Rnd 13"))
        __sortingEnabled = self.round_score.isSortingEnabled()
        self.round_score.setSortingEnabled(False)
        self.round_score.setSortingEnabled(__sortingEnabled)
        self.menuSpades.setTitle(_translate("MainWindow", "Spades"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
